/* Mesh Converter */

((global, $)=>{
  "use strict";

  let result_container
  let fileinput

  const CATEGORY_LOADING_FILE = 0
  const CATEGORY_PARSING_XML = 1
  const CATEGORY_CONVERTING = 2
  const CATEGORY_BUILD_LUA = 3
  const CATEGORY_LABELS = ['Loading File', 'Parsing XML', 'Converting', 'Building Lua']

  

    $(window).on('load', ()=>{
	    fileinput = $('#file')
	    result_container = $('#result-container')

	    $('#precision').on('change', updatePrecisionInformation)
	    updatePrecisionInformation()

	    fileinput.on('change', (evt)=>{
	        checkFileInformation()
	    })
	    checkFileInformation()
    })

    function updatePrecisionInformation(){
    	$('label[for="precision"]').html($('#precision').val())
    }

	function checkFileInformation(){

	    let file = fileinput.get(0).files[0]
	    if(file){
	        console.log(file)
	        $('#selection #file-information').html(''+file.name + '   ' + parseInt(file.size / 10000.0) / 100 + ' MB')
	        $('#selection #process-button').removeAttr('disabled')
	    } else {
	        $('#selection #file-information').html('-')
	        $('#selection #process-button').attr('disabled', 'true')
	    }
	}

	function convertBlob(){
	    if(! fileinput){
	        return console.error('Fileinput is not defined!')
	    }
	    let file = fileinput.get(0).files[0]
	    if(!file){
	        return handleFileError('no file choosen')
	    }
	    startAnalyzingProgress()
	    let fr = new FileReader()
	    fr.onload = (data)=>{
	        handleFileProgress(true)
	        processXML(fr.result).then(()=>{
	        	stopAnalyzingProgress()
		        $('#selection').fadeOut(200, ()=>{
		            $('#result-container').fadeIn(200)
		        })
		    })
	    }
	    fr.onerror = (evt)=>{
	      handleFileError(evt)
	    }

	    fr.onprogress = (evt)=>{
	      handleFileProgress(evt)
	    }
	    log('reading blob (' + file.size / 1000 + ' kB)')
	    fr.readAsText(file)
	}

	function processXML(xml){
	  	return new Promise((fulfill, reject)=>{
		    const options = {
		        attributeNamePrefix : "",
		        ignoreAttributes : false,
		        ignoreNameSpace : false,
		        allowBooleanAttributes : true,
		        parseNodeValue : true,
		        parseAttributeValue : false,
		        trimValues: true,
		        parseTrueNumberOnly: false,
		        attrValueProcessor: a=>a,
		        tagValueProcessor : a=>a
		    }



		    handleAnalyzingProgress(CATEGORY_PARSING_XML, 0)
		    let obj = XMLParser.parse(xml, options);
		    handleAnalyzingProgress(CATEGORY_PARSING_XML, 1)

		    console.log(obj)


		    

		    convert(obj).then((converted)=>{
		    	result_container.html('')

		    	buildLua(converted, $('#mesh-compression').prop('checked'), result_container).then(()=>{
				    setTimeout(()=>{
				    	fulfill()
				    },1)
		    	})

		    })
		})
	}
	
	function convert(obj){
		return new Promise((fulfill, reject)=>{

			handleAnalyzingProgress(CATEGORY_CONVERTING, 0)

			let meshesDone = 0


		    let compat = checkCompatibility(obj)
		      
		    if(compat !== true){
		        handleFileError('not compatible: ' + compat)
		        fulfill()
		        return
		    }
		    

			let meshes = obj.ASSIMP.Scene.MeshList.Mesh
			if(meshes instanceof Array == false){
                meshes = [meshes]
            }

			let converted = []

			for(let m of meshes){

				if(m.types.indexOf('triangles') < 0){
					handleError('mesh is not made of triangles!')
					return reject()
				}

				let faces = []

				for(let f of m.FaceList.Face){
					let it = []

					let split = f['#text'].split(' ')

					for(let s of split){
						it.push(parseInt(s))
					}

					faces.push(it)
				}

				console.log('faces', faces)


				let biggestLength = 0

				let positions = []

				let postexts=m.Positions['#text'].split('\r\n')
				for(let p of postexts){
					let it = []

					let rip = p.replace(/[\t\s]+/g, ' ').trim()
					let split = rip.split(' ')
					for(let s of split){
						it.push(parseFloat(s))
					}

					let length = get3DLength(it)
					if(length > biggestLength){
						biggestLength = length
					}

					positions.push(it)
				}

				console.log("positions", positions)

				let precision = parseInt($('#precision').val())
				if ($('#mesh-compression').prop('checked')){
					//force precision to be two digits
					precision=2
				}

				//normalize and adjust precision
				for(let i in positions){
					positions[i][0] = cutPrecision(positions[i][0] / biggestLength, precision)
					positions[i][1] = cutPrecision(positions[i][1] / biggestLength, precision)
					positions[i][2] = cutPrecision(positions[i][2] / biggestLength, precision)
				}

				console.log("normalized positions", positions)

				let triangles = []

				for(let f of faces){
					if(f.length == 3){
						triangles.push([
							[].concat(positions[f[0]]),
							[].concat(positions[f[1]]),
							[].concat(positions[f[2]])
						])
					}
				}

				console.log("triangles", triangles)

				converted = converted.concat(triangles)

				meshesDone++
				handleAnalyzingProgress(CATEGORY_CONVERTING, meshesDone/meshes.length)
			}


			handleAnalyzingProgress(CATEGORY_CONVERTING, 1)
			fulfill(converted)
		})
	}


	function checkCompatibility(obj){
		if( !obj.ASSIMP || obj.ASSIMP.format_id !== '1' || !obj.ASSIMP.Scene || !obj.ASSIMP.Scene.MeshList || (obj.ASSIMP.Scene.MeshList.Mesh instanceof Array == false && obj.ASSIMP.Scene.MeshList.Mesh instanceof Object == false)){
			return 'format is not matching, must be ASSIMP.format_id="1"'
		}

		return true
	}

	function buildLua(converted, minifiedWithPrecisionOfTwoDigits, container){
		return new Promise((fulfill, reject)=>{

			handleAnalyzingProgress(CATEGORY_BUILD_LUA, 0)

			let code=''

			if(minifiedWithPrecisionOfTwoDigits){
				
				let lines = []
			
				for(let c of converted){
					let chars=[]
					for(let t of c){
						for(let k in t){
							chars.push(encode(t[k]))
						}
					}
					lines.push(chars.join(''))
				}

				let i = 0
				while((lines.length - i) > 334){
					make(i,i+334)
					i+=334
				}
				make(i,lines.length-1)

				function make(start, end){
					let t = $('<textarea>"' + lines.slice(start, end).join('') + '"</textarea>')
					container.append(t)
				}
			} else {
				let code='meshCube={\n'

				let lines = []

				for(let c of converted){
					let tmp = []
					for(let t of c){
						let values = t

						tmp.push('{' + values.join(',') + '}')
					}

					lines.push('{' + tmp.join(',') + '}')
				}

				
				code += lines.join(',\n') + '\n'

				code = code.substring(0, code.length-1) + '\n}'

				container.append($('<textarea>').val(code))
			}
			
			handleAnalyzingProgress(CATEGORY_BUILD_LUA, 1)
			
			fulfill()
		})

		/* $ is used for newline */
		function encode(number){
			let step=1/(126-36)*2
			console.log('can use up to ', 1/step * 2, 'chars')
			let cleannumber = number/step*step
			let code = Math.floor(cleannumber*(126-36)/2)+(126-36)/2+36
			code = code == 92 ? 33 : code
			code = code == 96 ? 35 : code
			return String.fromCharCode(code)
		}

		
		function decode(char){
			let code = char.charCodeAt(0)
			code = code == 33 ? 92 : code
			code = code == 35 ? 96 : code

			return (code - (126-36)/2 - 36) / (126-36)*2
		}	

	}



  function startAnalyzingProgress(){
    $('#analyze-progress').show()
    $('#analyze-progress .category').html(CATEGORY_LABELS[0])
    $('#analyze-progress .progress').html('0 %')
    $('#analyze-progress .step').html('1/' + CATEGORY_LABELS.length)
  }

  function stopAnalyzingProgress(){
    $('#analyze-progress').hide()
    $('#analyze-progress .category').html('')
    $('#analyze-progress .progress').html('')
    $('#analyze-progress .step').html('')
  }

  function handleAnalyzingProgress(category, progress){
    //console.log('handleAnalyzingProgress', category, progress)
    $('#analyze-progress .category').html(CATEGORY_LABELS[category])
    $('#analyze-progress .progress').html( parseInt(progress * 100.0) + ' %' )
    $('#analyze-progress .step').html(category+1 + '/' + CATEGORY_LABELS.length)
  }


  function handleError(msg){
  	console.error(msg)
    $(result_container).html(msg.toString())
  }

  function handleFileError(evt){
    console.error(evt)
   	handleError(evt.toString())
  }

  function handleFileProgress(evt){
    if(evt === true){
      handleAnalyzingProgress(CATEGORY_LOADING_FILE, 1)
      $(result_container).css('cssText', '') 
      return
    }
    handleAnalyzingProgress(CATEGORY_LOADING_FILE, evt.loaded / evt.total)
    let percent = parseInt(evt.loaded / evt.total * 10000) / 100.0
    $(result_container).css('background', 'linear-gradient(to right, blue 0%, blue ' + percent + '%, white ' + (percent+0.01) + '%, white 100%)')
  }

  function log(msg){
    let args = []
    for(let a of arguments){
      args.push(a)
    }
    console.log.apply(console, ['ModelAnalyzer:'].concat(args))
  }



  function get3DLength(vector){
	return Math.sqrt( vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2] )
  }

  function cutPrecision(number, digitsAfterDot){
  	return Math.floor(number * (10**digitsAfterDot) )/(10**digitsAfterDot)
  }

  function checkSizes(){
    let min = Math.min($(window).height(), $(window).width()) / 2

    $('#selection').css({width: min+'px', height: min+'px'})
  }

  $(window).on('load', checkSizes)
  $(window).on('resize', checkSizes)
  

  global.MeshConverter = {
    convertBlob: convertBlob
  }

})(window, jQuery)
